<nav class="bt-gh navbar navbar-default" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#gn-header">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">
				<h1>British Travel Advantage</h1>
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="gn-header">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="http://www.britishtravelagency.co.uk" target="_blank">Home</a></li>
				<li><a href="http://status.britishtravelagency.co.uk">Service Status</a></li>
		        <li><a href="{{ URL::route('account-login') }}">Login</a></li>
		        <li><a href="{{ URL::route('contact-form') }}">Contact</a></li>
		        <li><a href="tel:004402081447031"><span style="border: 2px solid #0099cc; padding: 8px 11px" class="call">020 814 47031</span></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container -->
</nav>
@if(Session::has('global'))
	<p class="bg-warning text-center" style="color: black; padding-top: 10px; padding-bottom: 10px;"><b>Information: </b>{{ Session::get('global') }}</p>
@endif