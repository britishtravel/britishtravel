<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>British Travel :: Error </title>
        @include('global.meta.header')
    </head>
    <body>
        @include('global.include.en-US.navigation')
            <div class="container">
                <div class="row title error">
                    <div class="col-md-12">
                        <h1>Oh no!</h1>
                        <p>It's like when a house guest comes at the most awkward time and everything goes wrong. It seems you have encountered an error while browsing this site.</p>
                        <p>Try reloading the page to see if the problem has been fixed, else visit <a href="http://status.britishtravelagency.co.uk" target="_blank">status.britishtravelagency.co.uk</a></p>
                        <a href="javascript:history.back();">Go back to the previous page</a> <span style="margin: 0 15px; font-weight: bold;">:</span> <a href="/">Go to our home page</a>
                    </div>
                </div>
            </div>
        @include('global.meta.footer')
    </body>
</html>