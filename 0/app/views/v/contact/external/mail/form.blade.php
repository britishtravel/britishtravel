<font face="lucida grande" color="#141414" size="3">
The following message has been generated because a visitor sent you a message through your website.<br><br>

- - - - - -<br><br>
<b>Name: </b>{{ $name }} <br>
<b>Tel: </b>{{ $telephone }} <br>
<b>Email: </b>{{ $email }} <br>
<b>Question: </b>{{ $question }}<br>
<b>Information: </b>{{ $detail }}<br>
- - - - - -<br><br><br><br>
This message was sent from: <br>
<b>IP Address: </b> {{ $ip }}<br>
<br>
<i>Which can be checked against <a href="http://insight.developka.com/">http://insight.developka.com/</a> for previous usage records. <br>
You can also visit <a href="http://agent.btagency.co.uk/log">http://agent.btagency.co.uk/log</a> to check the most recent development updates. <br>
Finally, you can visit <a href="http://status.britishtravelagency.co.uk/">http://status.britishtravelagency.co.uk/</a> to check if there are any known service issues. <br>
<br>
<br>
<hr>
Note: <br>
Information below has been selected from your data base because the email used in the contact form submission was found as a registered user.<br>
<font size="2" color="#999">If there is no information below, no information was found in the database.</font>
<br>
<br>
<code><pre>{{ $db->id }}</pre></code> <br>
<code><pre>{{ $db->username }}</pre></code> <br>
<code><pre>{{ $db->first_name }}</pre></code> <br>
<code><pre>{{ $db->last_name }}</pre></code> <br>
<code><pre>{{ $db->subscription_start }}</pre></code> <br>
<code><pre>{{ $db->subscription_end }}</pre></code> <br>
<code><pre>{{ $db->email }}</pre></code> <br>