<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>British Travel :: IDSM has moved!</title>
        @include('global.meta.header')
    </head>
    <body>
        @include('global.include.en-US.navigation')
            <div class="container">
                <div class="row title error">
                    <div class="col-md-12">
                        <h1>WOAH</h1>
                        <p><b>This page has moooooved!</b> to a different location!<br>To log into the platform, please visit <a href="http://login.britishtravelagency.co.uk">login.britishtravelagency.co.uk</a>.</p>
                        <a href="javascript:history.back();">Go back to the previous page</a> <span style="margin: 0 15px; font-weight: bold;">:</span> <a href="http://login.britishtravelagency.co.uk">Go to the login page</a>
                    </div>
                </div>
            </div>
        @include('global.meta.footer')
    </body>
</html>