<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>British Travel :: Create Account</title>
        @include('global.meta.header')
    </head>
    <body>
        @include('global.include.en-US.navigation')
            <div class="container">
                <div class="row title">
                    <div class="col-md-12">
                        <h1>Create a User Account</h1>
                    </div>
                </div>
                <div class="row" style="padding-bottom: 30px;">
                    <form role="form" action="{{ URL::route('account-create-post') }}" method="post">
                        <div class="col-md-6">
                            <label for="aUID">Agent ID</label>
                            <input type="text" name="aUID" class="form-control input-lg">
                        </div>
                        <div class="col-md-6">
                            <label for="aPWD">Agent Password</label>
                            <input type="text" name="aPWD" class="form-control input-lg">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">
                                    Email Address
                                    @if($errors->has('email'))
                                        <p class="text-danger">{{ $errors->first('email') }}</p>
                                    @endif
                                </label>
                                <input type="email" class="form-control input-lg" name="email" placeholder="Enter a unique email" {{ (Input::old('email')) ? 'value="' . Input::old('email') . '"' : '' }}>
                            </div>
                            <div class="form-group">
                                <label for="username">
                                    Customer Number
                                    @if($errors->has('username'))
                                        <p class="text-danger">{{ $errors->first('username') }}</p>
                                    @endif
                                </label>
                                <input type="text" style="color: #aa5555;" class="form-control input-lg" name="username">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name">
                                    First Name
                                    @if($errors->has('first_name'))
                                        <p class="text-danger">{{ $errors->first('first_name') }}</p>
                                    @endif
                                </label>
                                <input type="text" class="form-control input-lg" name="first_name" placeholder="First Name">
                            </div>
                            <div class="form-group">
                                <label for="last_name">
                                    Last Name
                                    @if($errors->has('last_name'))
                                        <p class="text-danger">{{ $errors->first('last_name') }}</p>
                                    @endif
                                </label>
                                <input type="text" class="form-control input-lg" name="last_name" placeholder="Last Name">
                            </div>
                            <div class="form-group">
                                <label for="subscription_start">
                                    Subscription Start Delay (number of days)
                                    @if($errors->has('subscription_start'))
                                        <p class="text-danger">{{ $errors->first('subscription_start') }}</p>
                                    @endif
                                </label>
                                <input type="text" class="form-control input-lg" name="subscription_start" placeholder="Subscription Start Delay" {{ (Input::old('subscription_start')) ? 'value="' . Input::old('subscription_start') . '"' : 'value="0"' }}>
                            </div>
                            <div class="form-group">
                                <label for="subscription_end">
                                    Subscription Period (number of years)
                                    @if($errors->has('subscription_end'))
                                        <p class="text-danger">{{ $errors->first('subscription_end') }}</p>
                                    @endif
                                </label>
                                <input type="text" class="form-control input-lg" name="subscription_end" placeholder="Subscription Period (1, 5, 10)" {{ (Input::old('subscription_end')) ? 'value="' . Input::old('subscription_end') . '"' : 'value="1"' }}>
                            </div>
                            {{Form::token() }}
                        </div>
                        <div class="col-md-12">
                            <button type="submit" style="margin: 25px 0;" class="btn btn-danger btn-lg btn-block">Create Account</button>
                        </div>
                    </form>
                </div>
            </div>
        @include('global.meta.footer')
    </body>
</html>