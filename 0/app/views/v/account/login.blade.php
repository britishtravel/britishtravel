<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>British Travel :: Login</title>
        @include('global.meta.header')
    </head>
    <body>
        @include('global.include.en-US.navigation')
            <div class="container">
                <div class="row title">
                    <div class="col-md-12">
                        <h1>Login to British Travel Advantage</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <form role="form" action="{{ URL::route('account-login-post') }}" method="post">
                            <div class="form-group">
                                <label for="username">
                                    Customer Number
                                    @if($errors->has('username'))
                                        <p class="text-danger">{{ $errors->first('username') }}</p>
                                    @endif
                                </label>
                                <input type="text" id="cid" class="form-control input-lg" name="username" placeholder="BTA770918" {{ (Input::old('username')) ? 'value="' . Input::old('username') . '"' : '' }}>
                            </div>
                            <div class="form-group">
                                <label for="email">
                                    Email address
                                    @if($errors->has('email'))
                                        <p class="text-danger">{{ $errors->first('email') }}</p>
                                    @endif
                                </label>
                                <input type="email" class="form-control input-lg" name="email" placeholder="john@appleseed.com" {{ (Input::old('email')) ? 'value="' . Input::old('email') . '"' : '' }}>
                            </div>
                            <div class="form-group">
                                <label for="password">
                                    Memorable Password
                                    @if($errors->has('password'))
                                        <p class="text-danger">{{ $errors->first('password') }}</p>
                                    @endif
                                </label>
                                <input type="password" class="form-control input-lg" name="password" placeholder="applejuice5">
                            </div>
                            <button type="submit" class="btn btn-default">Login to Advantage</button>
                            {{ Form::token() }}
                        </form>
                    </div>
                    <div class="col-md-6">
                        <h3>What do I need to do?</h3>
                        <p>In the three boxes provided (customer number, email and password), please provide the email and password used when purchasing your subscription, and the customer number provided. Alternatively, you can call the number atop this screen to speak to a representative if you're having trouble logging in, or if you've forgotten your password.</p>
                        <p>Having a problem logging in? Check the <a href="http://status.britishtravelagency.co.uk/">platform status</a> to see if there are any service issues.</p>
                    </div>
                </div>
            </div>
        @include('global.meta.footer')
        <script type="text/javascript">
            $(document).ready(function()
            {
                $("#cid").click(function()
                {
                    var input = $("#cid");
                    input.val("BTA");
                });
            });
        </script>
    </body>
</html>