<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="home" href="http://www.britishtravelagency.co.uk/" />
<link rel="stylesheet" type="text/css" href="/resources/external/bootstrap/styles/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/resources/global/styles/base.css" />
<link rel="stylesheet" type="text/css" href="/resources/global/styles/navigation.css" />
<link rel="stylesheet" type="text/css" href="/resources/global/styles/enhanced.css" />
<link rel="stylesheet" type="text/css" href="/resources/global/styles/fonts.css" />